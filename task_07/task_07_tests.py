"""These are unittests for task_07"""
import unittest
from task_07 import solution


class HorseTests(unittest.TestCase):
    """This class for unittests who verify all possible coordinates."""

    def test_horse(self):
        """This is function for verify all possible coordinates."""
        result_coord = [[2, 'c'], [2, 'e'],
                        [6, 'c'], [6, 'e'],
                        [3, 'b'], [5, 'b'],
                        [3, 'f'], [5, 'f']]
        self.assertEqual(solution.coord_horse("4d"), result_coord)


if __name__ == '__main__':
    unittest.main()
