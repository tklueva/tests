"""solution for palindrome"""


def pol(number):
    """
    This function for verify that number is palindrome or not
    :param number: Function gets number
    :return: function returns True in case number is palindrome
             False in case number is not palindrome.
    """
    num = str(number)
    i = 0
    j = len(num)-1
    while i <= j:
        if num[i] == num[j]:
            i = i + 1
            j = j - 1
        else:
            return False
    return True
