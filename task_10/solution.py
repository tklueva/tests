"""This function for count middle sum of array"""


def arithmetic_mean(arr):
    """
    This function count the arithmetic mean of array.
    :param arr: it gets array.
    :return:Function returns the arithmetic mean.
    """
    summa = 0
    for i in arr:
        summa += i
    return summa / len(arr)
