"""These unittests for verify sequence."""
import unittest
from task_02 import solution
from task_02 import compare_string


class ISSequenceTests(unittest.TestCase):
    """
    This class for unittests.
    """

    def test_is_sequence(self):
        """
        This test verify sequence is right
        :return:True if sequence is right
        """
        my_seq = ['1', '2a', '3b', '4a', '5', '6ab', '7', '8a', '9b', '10a', '11', '12ab']
        seq = solution.my_sequence(13)
        self.assertTrue(compare_string.compare_str(my_seq, seq))

    def test_not_sequence(self):
        """
        This test verify sequence isn't right
        :return:False if sequence isn't right
        """
        m_seq = ['1', '2a', '3b']
        seq = solution.my_sequence(19)
        self.assertFalse(compare_string.compare_str(m_seq, seq))


if __name__ == '__main__':
    unittest.main()
