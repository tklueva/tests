"""This is a solution to verify the number."""


def convert_number(number):
    """
    This function converts number to array
    :param number: function gets number
    :return: function returns array of digits
    """
    massiv = []
    while number != 0:
        ostatok = number % 10
        number = number // 10
        massiv.append(ostatok)
    if (len(massiv) % 2) != 0:
        return False
    return massiv


def happy_number(massiv):
    """
    This function verify that number is happy number.
    :param massiv: Function gets array of digits.
    :return: Function returns True in case number is happy number
             False in case number isn't happy number.
    """
    i = 0
    sum1 = 0
    sum2 = 0
    while i < len(massiv):
        if i < (len(massiv) / 2):
            sum1 = sum1 + massiv[i]
        else:
            sum2 = sum2 + massiv[i]
        i = i + 1
    return sum1 == sum2
