"""This is solution for even/odd number"""


def is_even(num):
    """ This function determines
    whether the number is even or odd.
    :param dig: the number that must be checked for even/odd
    :return: True in case number is even
    False in case number is odd
    """
    return num % 2 == 0
