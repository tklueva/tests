"""These are unittests"""


import unittest
from task_04.solution import happy_number, convert_number


class IsHappyTests(unittest.TestCase):
    """
    This unittest verify that number is happy number
    """

    def test_ishappy(self):
        """This is unittest for happy number"""
        self.assertTrue(happy_number(convert_number(1221)))

    def test_not_happy(self):
        """This is unittest for not happy number"""
        self.assertFalse(happy_number(convert_number(12212)))


if __name__ == '__main__':
    unittest.main()
