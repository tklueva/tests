"""These are unittests for binary happy number"""
import unittest
from task_09.solution import binary_happy
from task_04.solution import happy_number


class BinaryTests(unittest.TestCase):
    """
    These unittests verify
    that number is binary happy number
    """
    def test_is_happy(self):
        """This test for happy number"""
        self.assertTrue(happy_number(binary_happy(45)))

    def test_not_happy(self):
        """This test for not happy number"""
        self.assertFalse(happy_number(binary_happy(47)))


if __name__ == '__main__':
    unittest.main()
