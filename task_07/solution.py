"""This is a function for handling coordinate"""


def coord_horse(coord):
    """
    This function for count of coordinates.
    :param coord: Function gets coordinate
    :return: Function returns list of coordinates.
    """
    hand_coord = {1: "a", 2: "b", 3: "c", 4: "d", 5: "e", 6: "f", 7: "g", 8: "h"}
    steps = [[-2, -1], [-2, 1], [2, -1], [2, 1], [-1, -2], [1, -2], [-1, 2], [1, 2]]
    position = [int(coord[0])]

    # the letter is interpreted as a number

    for k in hand_coord:
        if hand_coord.get(k) == coord[1]:
            position.append(k)
    j = 0

    # creating empty array of coordinates

    variable_step = []

    # algorithm for create possible steps

    for i in range(1, 9):
        variable_step.append([0]*2)
    i = 0
    for row in steps:
        for bukva in row:
            coordinate = position[j] + bukva
            if 8 > coordinate > 0:
                if j == 1:
                    variable_step[i][j] = hand_coord.get(coordinate)
                else:
                    variable_step[i][j] = coordinate
                j = j + 1
        i = i + 1
        j = 0
    print(variable_step)
    return variable_step
