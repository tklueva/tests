"""This function verifies work with file."""
import os


def open_file(name_file):
    """
    This function tries to open file
    :param name_file: Function gets name of file
    :return: Function returns dictionary or
             False in case error received
    """
    try:
        with open(name_file, encoding='utf8') as fail:
            len_num, str_line = count_line(fail)
            properties_text = {'len_num': len_num,
                               'word_num': count_word(str_line),
                               'size_file': size_file(name_file)}
            print(properties_text)
            return properties_text
    except IOError as error1:
        print("MY Error ", error1)
    except UnicodeError as error2:
        print("MY Unicode error ", error2)
    return False


def count_word(list_str):
    """
    This function counts words
    :param list_str: Function gets list of string
    :return: number of words
    """
    num = 0
    for i in list_str:
        num += len(i.split())
    return num


def count_line(name_file):
    """
    This function counts lines
    :param name_file: Function gets file
    :return: list of line
    """
    line_list = name_file.readlines()
    return len(line_list), line_list


def size_file(name_file):
    """
    This function determines size of file
    :param name_file: Function gets file
    :return: number(byte)
    """
    size_f = str(os.path.getsize(name_file))
    return size_f
