"""This is a unittest for task_06"""


import unittest
from task_06 import solution


class DigitTests(unittest.TestCase):
    """This class contains unittests for verify number"""

    def test_isdouble(self):
        """
        This test for number who has same digits
        :return: True in case number has same digits
        """
        self.assertTrue(solution.verified_number(123345))

    def test_isnot(self):
        """
            This test for number who hasn't same digits
            :return: False in case number hasn't same digits
        """
        self.assertFalse(solution.verified_number(123))


if __name__ == '__main__':
    unittest.main()
