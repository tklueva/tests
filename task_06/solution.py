"""This is a solution for same digits for number"""


def verified_number(number):
    """
    This function finds same digits
    :param number: Function gets number and pars to list
    :return: True in case number has same digits
             False in case namber hasn't same digits
    """
    num_str = str(number)
    count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in num_str:
        count[int(i)] += 1
    for j in count:
        if j > 1:
            return True
    return False
