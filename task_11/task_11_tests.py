"""These are unittests for the verification of medium value"""
import unittest
from task_11.solution import mediana, sort_arr


class MediumTests(unittest.TestCase):
    """
    These unittests verify that list is median value of array
    """

    def test_even_array(self):
        """This is function for verify even array"""
        ev_arr = [1, 4, 8, 3, 5, 10]
        self.assertEqual(mediana(sort_arr(ev_arr)), [5, 4])

    def test_odd_array(self):
        """This is function for verify odd array"""
        odd_arr = [3, 5, 7, 1, 0]
        self.assertEqual(mediana(sort_arr(odd_arr)), [3])


if __name__ == '__main__':
    unittest.main()
