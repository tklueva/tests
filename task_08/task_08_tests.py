"""These are unittests for verify palindrome."""

import unittest
from task_08 import solution


class PolTests(unittest.TestCase):
    """
    This class for unittests
    """

    def test_pol(self):
        """Test for right palindrome."""
        self.assertTrue(solution.pol(12321))

    def test_nopol(self):
        """Test for not right palindrome."""
        self.assertFalse(solution.pol(2562))


if __name__ == '__main__':
    unittest.main()
