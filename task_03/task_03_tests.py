"""These are unittests"""
import unittest
from task_03 import solution


class IsFibTests(unittest.TestCase):
    """
    This is unittest for verify that it is Fibonacci sequence.
    """
    def test_fib(self):
        """
        This is test for right Fibonacci sequence.
        :return: True in case sequence is Fibonacci
        """
        num_fib = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
        self.assertTrue(solution.number_fib(10) == num_fib)

    def test_not_fib(self):
        """
        This is test for not right Fibonacci sequence.
        :return: False in case sequence isn't Fibonacci
        """
        num_fib = [0, 1, 1, 2, 3, 5, 8, 13, 21, 50]
        self.assertFalse(solution.number_fib(10) == num_fib)


if __name__ == '__main__':
    unittest.main()
