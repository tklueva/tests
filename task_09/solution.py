"""This function converts number to binary number."""


def binary_happy(num):
    """
    This function converts number to binary number.
    :param num: Function gets number.
    :return: Function returns digit list who is binary number.
    """
    x_num = num
    result_number = []
    while x_num > 1:
        result_number.append(x_num % 2)
        x_num = x_num // 2
    result_number.append(x_num)
    return result_number
