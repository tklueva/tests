"""These unittests for even/odd number"""
import unittest
from task_01 import solution


class OldEvenTests(unittest.TestCase):
    """ This class for tests
    that it verifies functions right work
    """

    def test_even(self):
        """ this unittest verifies even number
        :return: True in case number is even
        """

        self.assertTrue(solution.is_even(10))

    def test_odd(self):
        """ this unittest verifies odd number
                :return: True in case number is odd
                """
        self.assertFalse(solution.is_even(5))


if __name__ == '__main__':
    unittest.main()
