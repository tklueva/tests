"""This is a unittest for verify roots of the equation."""


import unittest
from task_05 import solution


class KvadratTests(unittest.TestCase):
    """
    This class for tests who verify roots of the equation.
    """

    def test_two(self):
        """
        This test for discriminant > 0
        :return: True in case values are equal
        """
        self.assertEqual(solution.descrim(1, 0, -16), (4, -4))

    def test_one(self):
        """
            This test for discriminant = 0
             :return: True in case values are equal
        """
        self.assertEqual(solution.descrim(1, 12, 36), (-6))

    def test_no(self):
        """
            This test for discriminant < 0
            :return: True in case values are equal
        """
        self.assertEqual(solution.descrim(5, 3, 7), False)


if __name__ == "__main__":
    unittest.main()
