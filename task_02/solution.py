"""This is solution for interesting sequence."""


def my_sequence(number):
    """
    This function generate sequence.
    :param number: length of sequence
    :return: function returns the generated sequence
    """
    sequence = []
    for i in range(1, number):
        if i % 6 == 0:
            sequence.append(str(i) + 'ab')
        elif i % 3 == 0 and i % 6 != 0:
            sequence.append(str(i) + 'b')
        elif i % 2 == 0 and i % 6 != 0:
            sequence.append(str(i) + 'a')
        else:
            sequence.append(str(i))
    return sequence
