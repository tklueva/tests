"""This function is solution for Fibonacci sequence."""


def number_fib(numb):
    """
    This function generate Fibonacci sequence.
    :param numb: length of sequence
    :return: Function return fibonacci sequence.
    """
    sequence_fib = []
    for i in range(0, numb):
        if i > 1:
            sequence_fib.append(sequence_fib[i - 1] + sequence_fib[i - 2])
        else:
            sequence_fib.append(i)
    return sequence_fib
