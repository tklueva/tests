"""These are functions for the find of medium value"""


def sort_arr(arr):
    """
    This function make sort of array.
    :param arr: Function gets array for sort
    :return: Function returns sorted array
    """
    for _ in range(len(arr)-1):
        for j in range(len(arr)-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return arr


def mediana(arr):
    """
    This function finds median value.
    :param arr:Function gets sorted array
    :return: Function returns median value.
    """
    if len(arr) % 2 == 0:
        median_value = [arr[(len(arr))//2], arr[(len(arr))//2 - 1]]
    else:
        median_value = [arr[(len(arr))//2]]
    return median_value
