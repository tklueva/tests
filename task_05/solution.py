"""This is a function for find x1 and/or x2"""
import math


def descrim(first, second, third):
    """
    This function counts discriminant
    :param first: first coefficient
    :param second: second coefficient
    :param third: third coefficient
    :return: Function returns roots or False
    """
    desc = second * second - 4 * first * third
    if desc > 0:
        return (int((-second + math.sqrt(desc)) / 2 * first),
                int((-second - math.sqrt(desc)) / 2 * first))
    if desc == 0:
        return int((-second + math.sqrt(desc)) / 2 * first)
    return False
