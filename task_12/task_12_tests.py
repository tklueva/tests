"""This class for unittests"""
import unittest
from task_12 import solution


class VerifyFileTest(unittest.TestCase):
    """
    These unittests for verify work with file
    """

    def test_file(self):
        """
        This positive test
        :return: number of line, number of word and size of file
        """
        path_file = r'C:\Users\SETaKL\PycharmProjects\tests\task_12\test.txt'
        result = {'len_num': 5, 'word_num': 24, 'size_file': '120'}
        self.assertEqual(solution.open_file(path_file), result)

    def test_binary_file(self):
        """
        This test for file without utf8 code
        :return: False
        """
        path_file = r'C:\Users\SETaKL\PycharmProjects\tests\task_12\test1.txt'
        print(self.assertFalse(solution.open_file(path_file)))

    def test_notfile(self):
        """
        This test for not existing file
        :return: False
        """
        path_file = r'C:\Users\SETaKL\PycharmProjects\tests\task_12\bzkkcmp'
        print(self.assertFalse(solution.open_file(path_file)))


if __name__ == '__main__':
    unittest.main()
