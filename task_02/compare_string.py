"""This is function for solution of interesting sequence."""


def compare_str(list_str1, list_str2):
    """
    This function compare two string
    :param list_str1: First string for comparing
    :param list_str2: Second string for comparing
    :return: True if string are equal.
            False if its aren't equal.
    """
    i = 0
    if len(list_str1) != len(list_str2):
        return False
    while i < len(list_str1):
        if list_str1[i] == list_str2[i]:
            i = i + 1
        else:
            return False
    return True
