"""These are unittests for verify middle sum of array"""
import unittest
from task_10 import solution


class MediumSumArray(unittest.TestCase):
    """
    These unittests verify that
    the number is the arithmetic mean of array
    """
    def test_verify_sum(self):
        """Function for verify middle sum of array"""
        mas = [1, 4, 8, 3]
        self.assertEqual(solution.arithmetic_mean(mas), 4)


if __name__ == '__main__':
    unittest.main()
